# CKEditor5 Custom Paste

## Introduction
This module enhances CKEditor 5 by filtering and customizing the content pasted into the editor. Allows administrators to specify tags that should be excluded from styling transformations during the paste process. This helps maintain content consistency and prevents unintended styling, ensuring a cleaner and more uniform text formatting experience within CKEditor 5.
## Requirements
- Drupal 9 or later
- CKEditor 5 module

## Installation
Install as you would normally install a contributed Drupal module. Visit [Installing modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

## Configuration
Navigate to the text format configuration page (Administration > Configuration > Content authoring > Text formats and editors). Here, you can enable the plugin and define the excluded tags in the "Ckeditor 5 Custom Paste" settings.

## Troubleshooting
If you encounter any issues, please check the Drupal and CKEditor 5 documentation. Ensure that the module is correctly installed and that your CKEditor build includes the necessary plugins.

## Maintainers
Current maintainers:
- [Mohamed Anis Taktak](https://www.drupal.org/u/matio89)

## License
[GPLv2](http://www.gnu.org/licenses/gpl-2.0.html)
